<topten>
    <h2>Top 10 Hottest Topic</h2>
    <ol>
        <li each={ post in hotPosts}><a href='/#posts/{ post.id }'>{ post.get("title") }</a></li>
    </ol>
    <script>
    var that = this;
    this.hotPosts = undefined;
    var uquery = new Parse.Query('UserInfo');
    uquery.equalTo('user', Parse.User.current()).first().then(function(result) {
        var level = result.get('levelnum');
        return Parse.Promise.as(level);        
    }).then(function(level){
        var query = new Parse.Query("UserPost");
        query.equalTo("difficulty", level);    
        query.descending("likes").limit(10);
        query.find().then(function(result) {
            console.log(result);
            that.hotPosts = result;
            that.update();
        });

    })

    </script>
</topten>