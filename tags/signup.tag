<signup>
    <form role="form" id="signupform">
        <div class="form-group">
            <h1>New to Reading Society? Join NOW!</h1>
            <input type="text" class="form-control" placeholder="Username" id="registername">
        </div>
        <div class="form-group">
            <input type="password" class="form-control" placeholder="Password" id="registerpwd">
        </div>
        <div class="form-group">
            <input type="email" class="form-control" placeholder="Email address" id="registeremail">
        </div>
        <button class="btn btn-default" onclick={ userSignUp }>Sign Up</button>
    </form>

    <script>
    this.userSignUp = function(event) {
        var user = new Parse.User();
        var name = $("#registername").val();
        var pass = $("#registerpwd").val();
        var email = $('#registeremail').val();
        
        user.set("username", name);
        user.set("password", pass);
        user.set('email', email);

        user.signUp(null, {
            success: function(user) {
                window.location.href = 'test.html';
            },
            error: function(user, error) {
                alert('signup error:' + error.message);
            }
        })
    };
    </script>

    <style scoped>
    #signupform {
        margin-top: 70px;
    }
    h1 {
        font-size: 20px;
        color: rgba(95, 60, 70, 1);
    }
    .form-control {
        width: 250px;
    }
    button {
        background-color: rgba(95, 60, 70, 1);
    }
    </style>    

</signup>
