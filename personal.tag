<personal>
    <div>
        <h1>{ "Welcome to your own page," + " " + user}</h1>
        <div id="showMyPic">{ myPic }</div>
        <h3>{ "Username: " + user }</h3>
        <h3>Self-Intro:</h3>
        <h4>{ selfIntro }</h4>
        <h3>Reading Interests:</h3>
        <h4>{ interest }</h4>
        <h3>English Level:</h3>
        <h4>{ engLevel }</h4>
        <button onclick={ editAll }>Edit</button>
        <button onclick="location.href='personalPage.html'">Go To My Personal Page</button>
        <!--Edit Info section-->
        <div id="editInfo" show={ x }>
            <div>
                <h3>Update your information.</h3>
                <div>
                    <div class="container">
                        <!-- self-intro -->
                        <form>
                            <h4>Self-Introduction:</h4>
                            <textarea id="selfContent" rows=8 cols=100 placeholder="Introduce Yourself here!"></textarea>
                            <h4>Choose your picture.<input id="myPic" type="file"></h4>
                        </form>
                        <!-- Interest -->
                        <div id="myInterest">
                            <h4>Reading Interest:</h4>
                            <label>
                                <input id="stem" type="checkbox" value="stem">STEM</label>
                            <label>
                                <input id="culture" type="checkbox" value="culture">Culture</label>
                            <label>
                                <input id="health" type="checkbox" value="health">Health</label>
                            <label>
                                <input id="entertainment" type="checkbox" value="entertainment">Entertainment</label>
                        </div>
                        <!-- English Level -->
                        <div id="myLevel">
                            <h4>English Level:</h4>
                            <button class="btn btn-warning" id="testLevel" onclick={ testLevel }>Test Your Level</button>
                            <div class="hidden" id="testEng">
                                <div>
                                    <embed src='https://learnenglishteens.britishcouncil.org/content' width="1000" height="800"></embed>
                                </div>
                                <button class="btn btn-success" id="testLevelTwo" onclick={ testLevelTwo }>Close The Test</button>
                            </div>
                            <h4 style="color:red">Report your level down here.</h4>
                            <p> 1 = A1~A2 (beginner);
                                <br>2 = A2~B1 ;
                                <br>3 = B1~B2 ;
                                <br>4 = B2~C1 ;
                                <br>5 = C1~C2 (advanced)</p>
                            <label class="radio-inline">
                                <input type="radio" name="levelNum" value="1">1
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="levelNum" value="2">2
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="levelNum" value="3">3
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="levelNum" value="4">4
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="levelNum" value="5">5
                            </label>
                            <p></p>
                        </div>
                        <button onclick={ update }>Update</button>
                        <button onclick={ cancelEdit }>Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
    this.x = false;

    var that = this;
    that.myInfo = undefined;
    var query = new Parse.Query("UserInfo");
    query.equalTo("user", Parse.User.current()).first().then(function(result) {
        that.myInfo = result;
        var username = Parse.User.current().get("username")
        that.user = username;
        that.interest = result.get("interest");
        that.engLevel = result.get("level")
        that.selfIntro = result.get("selfIntro")
        var file = result.get("file");
        var url = file.url();
        var img = ""
        img = "<img src='" + url + "'>";
        $("#showMyPic").html(img)
        that.update();
    })

    this.editAll = function(event) {
        this.x=true;
    }
    this.cancelEdit = function(event) {
        this.x=false;
    }

    this.update = function(event) {
        var introContent = $("#selfContent").val();
        that.myInfo.set("selfIntro", introContent);
        var readingInterest = $('#myInterest input:checked').map(function() {
            return $(this).val();
        }).toArray();
        that.myInfo.set("interest", readingInterest);
        var myEngLevel = $('[name="levelNum"]:checked').val();
        var myLevelNum = Number(myEngLevel);
        that.myInfo.set("level", myEngLevel);
        that.myInfo.set("levelnum", myLevelNum);
        var fileElement = $("#myPic")[0];
        var filePath = $("#myPic").val();
        var fileName = filePath.split("\\").pop()
        var file = fileElement.files[0];
        var newFile = new Parse.File(fileName, file);
        newFile.save({
            success: function() {
                alert("picture updated")
            },
            error: function(error) {
                alert("File Save Error:" + error.message)
            }
        }).then(function(file) { //save pic first and then save other info
            that.myInfo.set("file", file);
            that.myInfo.save({
                success: function() {
                    alert("info updated!");
                    window.location.href = 'userprofile.html';
                },
                error: function(error) {
                    alert("Error:" + error.message);
                }
            })
        })
    }
    </script>
</personal>
